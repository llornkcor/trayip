#include "dialog.h"
#include <QApplication>
#include <QMessageBox>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("IPTray"),
                              QObject::tr("No system tray on this system."));
        return 1;
    }
    QApplication::setQuitOnLastWindowClosed(false);
    Dialog w;
    w.show();

    return a.exec();
}
