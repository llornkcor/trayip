#include "dialog.h"
#include "ui_dialog.h"

#include <QMenu>
#include <QMessageBox>
#include <QAction>
#include <QCoreApplication>
#include <QCloseEvent>
#include <QTimer>
#include <QHostInfo>
#include <QDebug>

#include <QNetworkInterface>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent)
{
    createActions();
    createTrayIcon();

    connect(trayIcon, &QSystemTrayIcon::activated, this, &Dialog::iconActivated);

    QTimer::singleShot(0,this,SLOT(hideApp()));
}

Dialog::~Dialog()
{
}

void Dialog::setVisible(bool visible)
{
    QDialog::setVisible(visible);
}

void Dialog::createTrayIcon()
{
    trayIcon = new QSystemTrayIcon(this);

    QString hAddress;

    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)
                && !address.isLinkLocal()) {
            hAddress =  address.toString();
        }
    }
    QIcon icon = QIcon(":/iptray.png");
    trayIcon->setIcon(icon);
    trayIcon->setToolTip(hAddress );

    trayIcon->show();
}

void Dialog::createActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, &QAction::triggered, this, &QWidget::hide);

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, &QAction::triggered, this, &QWidget::showMaximized);

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, &QAction::triggered, this, &QWidget::showNormal);

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);
}

void Dialog::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    QString hAddress;

    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)
                && !address.isLinkLocal()) {
            hAddress =  address.toString();
        }
    }
    QMessageBox::information(this,"", " IP Address " + hAddress + "" );
}

void Dialog::hideApp()
{
    if (trayIcon->isVisible()) {
        hide();
    }
}

void Dialog::setIcon(int /*index*/)
{
    QIcon icon = QIcon(":/iptray.png");
    trayIcon->setToolTip("IP here");
    trayIcon->setIcon(icon);
    trayIcon->show();
}

void Dialog::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible()) {
        hide();
        event->ignore();
    }
}

